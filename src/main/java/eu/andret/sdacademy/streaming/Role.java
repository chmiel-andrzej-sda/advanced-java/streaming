package eu.andret.sdacademy.streaming;

public enum Role {
	WORKER,
	MANAGER,
	HR,
	ACCOUNTANT,
	PR
}
