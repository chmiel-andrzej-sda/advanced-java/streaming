package eu.andret.sdacademy.streaming;

import java.util.List;

public record Company(String name, List<Employee> employees) {
}
